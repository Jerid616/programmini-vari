#include<stdio.h>
int Ackermann (int m, int n){
	if(m==0){
		n=n+1;
		return n;
	} else if(m>0 && n==0) {
		return Ackermann(m-1, 1);
	} else if(m>0 && n>0) {
		return Ackermann(m-1, Ackermann(m, n-1));
	}

	return -1;
}

int main(void){
	int m, n;
	printf("Inserisci il primo numero: ");
	scanf("%d", &m);
	printf("Inserisci il secondo numero: ");
	scanf("%d", &n);
	int a=Ackermann(m,n);
	printf("Risultato: %d\n", a);
	return 0;
}
